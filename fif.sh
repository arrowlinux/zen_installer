#!/bin/bash

# Written by Elias Walker & anonymous or unknown contributors   

CHECKED_NET=false
#################################################################


load_bcm()
{
    zenity --info --width=200 --text "Broadcom Wireless Setup" "\nLoading broadcom wifi kernel modules please wait...\n" 0
    rmmod wl >/dev/null 2>&1
    rmmod bcma >/dev/null 2>&1
    rmmod b43 >/dev/null 2>&1
    rmmod ssb >/dev/null 2>&1
    modprobe wl >/dev/null 2>&1
    depmod -a >/dev/null 2>&1
    BROADCOM_WL=true
}

chk_connect()
{
    if [[ $CHECKED_NET == true ]]; then
        zenity --warning --width=200 --timeout=5 --text "Network Connect Fail\nVerifying network connection\n" 1
    else
        zenity --info --width=300 --timeout=5 --text "Network Connect\nChecking connection to https://www.archlinux.org\n" 1
        CHECKED_NET=true
    fi
    curl -sI --connect-timeout 5 'https://www.archlinux.org/' | sed '1q' | grep -q '200'
}

net_connect()
{
    if ! chk_connect; then
        if [[ $(systemctl is-active NetworkManager) == "active" ]] && hash nmtui >/dev/null 2>&1; then
            tput civis
            printf "\e]P1191919"
            printf "\e]P4191919"
 #           nmtui-connect
            printf "\e]P1D15355"
            printf "\e]P4255a9b"
            chk_connect || return 1
        else
            return 1
        fi
    fi
    return 0
}

system_checks()
{
    if [[ $(whoami) != "root" ]]; then
        zenity --error --width=200 --text "Not Root \nThis installer must be run as root or using sudo.\n\nExiting..\n"
        exit 1
    elif ! grep -qw 'lm' /proc/cpuinfo; then
        zenity --error --width=200 --text "Not x86_64 \nThis installer only supports x86_64 architectures.\n\nExiting..\n"
        exit 1
    fi

    grep -q 'BCM4352' <<< "$(lspci -vnn -d 14e4:)" && load_bcm

    if ! net_connect; then
        zenity --error --width=300 --text "Not Connected \nThis installer requires an active internet connection.\n\nExiting..\n"
        exit 1
    fi
}


system_checks





zenity --question --width=280 --title="Arrow Installer" --text  "This will walk you through an Arch install \n\nWARNING \nYour next chance to quit is 35 windows away! \n\nAre You Ready?"
   if [[ "$?" = "1" ]]
      then exit
      else sudo /home/liveuser/zen_installer-master/zif
   fi 


